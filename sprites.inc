<?php

/**
 * @file
 * CSS Sprite Generator 
 * Functions needed in order to handle sprite generation.
 * developed by Tj Holowaychuk - www.350designs.com
 */  
 
/**
* Initialize sprite generation processes.
*/
function sprites_generate() {
  $sprites_per = variable_get('sprites_per_image',  0);
  $max_width   = variable_get('sprites_max_width',  SPRITES_MAX_WIDTH_DEFAULT);               
  $max_height  = variable_get('sprites_max_height', SPRITES_MAX_HEIGHT_DEFAULT);     
  $offsetx     = variable_get('sprites_offsetx', 5);         
  $offsety     = variable_get('sprites_offsety', 5);         
  $types       = array('gif', 'png', 'jpg');    
  
  sprites_collection_delete_all();
  
  foreach($types AS $type){
    $images = sprites_images_get_filepaths($type);           
    
    if (!count($images)){
      continue;
    }          
              
    // Validate images
    sprites_images_validate($images, $max_width, $max_height);
               
    // Sprites per collection limit
    if ($sprites_per){         
      $chunks = array_chunk($images, $sprites_per);  
      foreach($chunks AS $i => $chunk){           
        $name  = 'sprites_' . ($i + 1) . '.' . $type; 
        $rules = _sprites_create($chunk, $max_width, $max_height, $name, 'sprites', FALSE, $offsetx, $offsety); 
        _sprites_generate($name, $rules, $set_css); 
      }
    }
    else {
      $rules = _sprites_create($images, $max_width, $max_height, 'sprites.' . $type, 'sprites', FALSE, $offsetx, $offsety);  
      _sprites_generate('sprites.' . $type, $rules, $set_css);    
    }  
  }                            
}                

/**
* Helper for sprites_generate();
* 
* @see sprites_generate()
*/
function _sprites_generate($name, $rules, $set_css) {  
  if (count($rules)){       
    // Save sprites object
    $collection = new stdClass();
    $collection->sid      = 0;
    $collection->filename = file_directory_path() . '/sprites/' . $name; 
    $collection->rules    = $rules; 
    sprites_collection_save($collection);
  }
}  
     
/**
* Helper for sprites_create();
* 
* @see sprites_create()
*/  
function _sprites_create($images, $max_width = SPRITES_MAX_WIDTH_DEFAULT, $max_height = SPRITES_MAX_HEIGHT_DEFAULT, $name = 'sprites.jpg', $dest = 'sprites', $validate_images = FALSE, $offsetx = 5, $offsety = 5){ 
  $rules    = array(); 
  $filename = $dest ? file_directory_path() . '/' . $dest . '/' . $name : file_directory_path() . '/' . $name; 
  $ext      = pathinfo($filename, PATHINFO_EXTENSION); 
  $_offsety = 0;
  
  // Due to not 'packing' sprites there is currently no
  // use for implementing offsetx.
  $offsetx = 0;
  
  // Destination
  if ($dest = file_create_path($dest)){
    if (!file_check_directory($dest, FILE_CREATE_DIRECTORY)){
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
  
  // Remove images that do not meet the criteria   
  if ($validate_images === TRUE){
    sprites_images_validate($images, $max_width, $max_height);
  }
  
  if (count($images)){
    // Generate CSS rules and offsets 
    foreach($images AS $image){ 
      $rules[$image->filename] = array(
          'image'      => $image,
          'selector'   => '.sprite-' . preg_replace('/[^a-zA-Z0-9]/is', '-', $image->name),
          'background' => $filename, 
          'width'      => $image->info[0],
          'height'     => $image->info[1],
          'offsetx'    => $offsetx,
          'offsety'    => $_offsety,
        );                 
      $_offsety += $image->info[1] + $offsety;  
    }

    // Generate sprites Image
    if (count($rules)){    
      // Find the maximum width used
      foreach($rules AS $rule){
        $widths[] = $rule['width'];
      }                   
      
      // Create image resource      
      $sprites = imagecreate(max($widths) + $offsetx, $_offsety);          
      
      // Image formats which support transparency should have an
      // irregular background color which is later considered transparent
      switch($ext){
        case 'gif':
        case 'png':
          $background_color = imagecolorallocate($sprites, 255, 0, 190);
          break;
        
        case 'jpg':
        case 'jpeg':
          $background_color = imagecolorallocate($sprites, 255, 255, 255); 
          break;
      }
      
      foreach($rules AS $rule){
        if (!file_exists($rule['image']->filename)){
          continue;
        }
              
        // Create source image resource
        switch($rule['image']->info['mime']){
          case 'image/jpg':
          case 'image/jpeg':
            $image = imagecreatefromjpeg($rule['image']->filename); 
            break;
            
          case 'image/png':
            $image = imagecreatefrompng($rule['image']->filename);
            break;
            
          case 'image/gif':
            $image = imagecreatefromgif($rule['image']->filename);
            break;
        }
                   
        // Copy source image to the offset position
        if (!imagecopy($sprites, $image, 0, $rule['offsety'], 0, 0, $rule['width'], $rule['height'])){
          drupal_set_message(t('Error copying image %filename.', array('%filename' => $rule['image']->filename)), 'error');    
        }
        
        @imagedestroy($image);
      }
      
      // Remove existing files
      file_delete($filename);
      
      // Create the appropriate image type based on the extension passed 
      switch($ext){
        case 'jpg':
        case 'jpeg':
          imagejpeg($sprites, $filename, 90); // @todo settings
          break;
          
        case 'png':
          imagecolortransparent($sprites, $background_color);
          imagepng($sprites, $filename, 0); // @todo settings
          break;
          
        case 'gif':
          imagecolortransparent($sprites, $background_color);
          imagepng($sprites, $filename); // @todo settings
          break;
      } 
      
      @imagedestroy($sprites); 
    }
  }
  
  drupal_set_message(t('!count !type sprites generated.', array('!count' => count($rules), '!type' => $ext)));        
  return $rules;                                
}

/**
* Ignore images that do not follow specific criteria.
*   
* Criteria:
*   - Within width and height limits.
* 
*   - Invoke hook_sprites_images_ignore();  
*     Array of image filenames ex: ('./misc/farbtastic/marker.png') 
*     which will be ignored and not included or re-written as CSS sprites.
* 
*   - Images should be ignored if they are already being used  
*     as sprites, animated gifs, or are simply rarely seen.
* 
* @param array $images    
*   Image objects.
* 
* @param int $max_width
* 
* @param int $max_height
*/
function sprites_images_validate(&$images, $max_width = SPRITES_MAX_WIDTH_DEFAULT, $max_height = SPRITES_MAX_HEIGHT_DEFAULT) { 
  if (count($images)){
    $ignore = module_invoke_all('sprites_images_ignore');
    foreach($images AS $i => $image){
      $image->info = @getimagesize($image->filename);
      if ($image->info[0] > $max_width || $image->info[1] > $max_height || in_array($image->filename, $ignore)){ 
        unset($images[$i]);  
      }
    }
  }   
}   

/**                               
* Get contents of all CSS files.
*      
* @return array  
*   Objects containing their CSS contents.
*/
function sprites_css_get() {
  $output = array();
  $files  = sprites_css_get_filepaths(); 
                 
  if (count($files)){
    foreach($files AS $file){
      if (file_exists($file->filename)){
        $file->contents = file_get_contents($file->filename);
        $output[] = $file; 
      }
    }
  } 
       
  return $output;
}

/**
* Get css files from directories that may contain them.
*/
function sprites_css_get_filepaths() { 
  // @todo look at other examples ./ should not be needed? if not replace other instances of ./ with /
  $d1 = file_scan_directory('./modules',  '\.css$');
  $d2 = file_scan_directory('./misc',     '\.css$');
  $d3 = file_scan_directory('./profiles', '\.css$');
  $d4 = file_scan_directory('./sites',    '\.css$');
  
  return @array_merge_recursive($d1, $d2, $d3, $d4);
}

/**
* Get image files from directories that may contain them.
*/
function sprites_images_get_filepaths($types) {
  $d1 = file_scan_directory('./modules',  '\.(' .$types. ')$');
  $d2 = file_scan_directory('./misc',     '\.(' .$types. ')$');
  $d3 = file_scan_directory('./profiles', '\.(' .$types. ')$');
  $d4 = file_scan_directory('./sites',    '\.(' .$types. ')$');
  
  return @array_merge_recursive($d1, $d2, $d3, $d4);
}


